<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\PrakriaClient;

class ProjectController extends Controller
{
    public function store(Request $request)
    {
        $project = new PrakriaClient();
        $project->project_title = $request->input('project_title');
        $project->owner = $request->input('owner');
        $project->starting_date = $request->input('starting_date');
        $project->expected_duration = $request->input('expected_duration');
        $project->save();
        return response()->json([
            'status' => 200,
            'message' => 'Project Added Successfully'
        ]);
    }
}
